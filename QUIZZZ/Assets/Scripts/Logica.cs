﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Quiz
{
    public string texto;
}

public class ClassResposta : Quiz
{
    public bool ecorreta;
    
    public ClassResposta(string a, bool b)
    {
        this.texto = a;
        this.ecorreta = b;
    }
}

public class ClassPergunta : Quiz
{ // CLASS PERGUNTAS
    public int pontos;
    public ClassResposta[] respostas;

    // METODO É PARA CRIAR AS PERGUNTAS
    public ClassPergunta(string t, int p)
    {
        this.texto = t;
        this.pontos = p;
    }

    // METODO PARA ADD RESPOSTA
    public void AddResposta(string a, bool b)
    {
        if (respostas == null)
        {
            respostas = new ClassResposta[4];
        }

        for (int i = 0; i < respostas.Length; i++)
        {
            if (respostas[i] == null)
            {
                respostas[i] = new ClassResposta(a, b);
                break;
            }
        }
    }

    // LISTA ou EXIBIR as respostas
    public void ListarRespostas()
    {
        if (respostas != null && respostas.Length > 0)
        {
            foreach (ClassResposta r in respostas)
            {
                Debug.Log(r);
            }
        }
    }

    // Checar resposta correta
    public bool ChecarRespostaCorreta(int pos)
    {
        if (respostas != null && respostas.Length > pos && pos >= 0)
        {
            return respostas[pos].ecorreta;
        }
        else
        {
            return false;
        }
    }

} // FIM CLASS PERGUNTAS


// CLASS LÓGICA - PRINCIPAL
public class Logica : MonoBehaviour
{ // INICIO CLASS LOGICA

    public Text tituloPergunta;
    public Button[] respostaBtn = new Button[4];
    public List<ClassPergunta> PerguntasLista = nw List<ClassPergunta>();

    private ClassPergunta perguntaAtual;

    public GameObject PainelCorreto;
    public GameObject PainelErrado;
    public GameObject PainelFinal;
    private int contagemPerguntas = 0;
    private float tempo;
    private bool carregarProximaPergunta;
    private int pontos = 0;


    void Start()
    { // METODO START

        ClassPergunta p1 = new ClassPergunta("Em guerras secretas qual personagem que ficou no planeta bélico, após o final da guerra ", 5);
        p1.AddResposta("Reed richards",false);
        p1.AddResposta("Ciclopes", false);
        p1.AddResposta("Capitão américa", false);
        p1.AddResposta("Coisa", true);

        ClassPergunta p2 = new ClassPergunta("Como e feito o adamantium", 3);
        p2.AddResposta("Por mineração", false);
        p2.AddResposta("Junção de vários materiais", true);
        p2.AddResposta("Queda de meteoro", false);
        p2.AddResposta("Origem mágica", false);

        ClassPergunta p3 = new ClassPergunta("Qual dessas heroínas já sofreu violência doméstica", 6);
        p3.AddResposta("Jean grey", false);
        p3.AddResposta("Sue Storm", false);
        p3.AddResposta("Vespa", true);
        p3.AddResposta("Psylocke", false);

        ClassPergunta p4 = new ClassPergunta("Quem usava a armadura do homem de ferro durante as guerras secretas", 5);
        p4.AddResposta("Tony Stark", false);
        p4.AddResposta("Norman Osborn", false);
        p4.AddResposta("Pepper Pots", false);
        p4.AddResposta("James Rhodes", true);

        ClassPergunta p5 = new ClassPergunta("Quem foi o primeiro ser humano a receber o nome de spawn", 5);
        p5.AddResposta("Al Simmons", false);
        p5.AddResposta("Cain", true);
        p5.AddResposta("Malebolgia", false);
        p5.AddResposta("Terry", false);

        ClassPergunta p6 = new ClassPergunta("Quais são os deuses que dão poder ao adão negro", 3);
        p6.AddResposta("Gregos", false);
        p6.AddResposta("Nórdicos", false);
        p6.AddResposta("Egípcios", true);
        p6.AddResposta("Africanos", false);

        ClassPergunta p7 = new ClassPergunta("O que a cor verde representa aos lanternas verde", 3);
        p7.AddResposta("Determinação", false);
        p7.AddResposta("Medo", false);
        p7.AddResposta("Força de vontade", true);
        p7.AddResposta("Raiva", false);

        ClassPergunta p8 = new ClassPergunta("Em piada mortal o que o batman faz após ouvir a piada de coringa", 3);
        p8.AddResposta("O matou", false);
        p8.AddResposta("Deixou ele fugir", false);
        p8.AddResposta("Riu", true);
        p8.AddResposta("O prendeu", false);

        ClassPergunta p9 = new ClassPergunta("Qual carreira kick ass seguiu ao final do quadrinho", 4);
        p9.AddResposta("Policial", true);
        p9.AddResposta("Médico", false);
        p9.AddResposta("Continuou como vigilante", false);
        p9.AddResposta("Contador", false);

        ClassPergunta p10 = new ClassPergunta("O que hellboy encontra no quarto do inferno no final do quadrinho", 5);
        p10.AddResposta("Uma criança", false);
        p10.AddResposta("Formas geométricas", true);
        p10.AddResposta("Uma coroa", false);
        p10.AddResposta("Sua mãe", false);



        PerguntasLista.Add(p1);
        PerguntasLista.Add(p2);
        PerguntasLista.Add(p3);
        PerguntasLista.Add(p4);
        PerguntasLista.Add(p5);
        PerguntasLista.Add(p6);
        PerguntasLista.Add(p7);
        PerguntasLista.Add(p8);
        PerguntasLista.Add(p9);
        PerguntasLista.Add(p10);

        perguntaAtual = p1;

        ExibirPerguntasNoQuiz();


    } // FIM METODO START

    void Update()
    {
        if (carregarProximaPergunta == true)
        {
            tempo += Time.deltaTime;
            if (tempo > 3)
            {
                tempo = 0;
                carregarProximaPergunta = false;
                ProximaPergunta();
            }
        }
    }


    private void ExibirPerguntasNoQuiz()
    {
        tituloPergunta.text = perguntaAtual.texto;
        respostaBtn[0].GetComponentInChildren<Text>().text = perguntaAtual.respostas[0].texto;
        respostaBtn[1].GetComponentInChildren<Text>().text = perguntaAtual.respostas[1].texto;
        respostaBtn[2].GetComponentInChildren<Text>().text = perguntaAtual.respostas[2].texto;
        respostaBtn[3].GetComponentInChildren<Text>().text = perguntaAtual.respostas[3].texto;
    }

    public void ProximaPergunta()
    {
        contagemPerguntas++;
        if (contagemPerguntas < PerguntasLista.Count)
        {
            perguntaAtual = PerguntasLista[contagemPerguntas];
            ExibirPerguntasNoQuiz();
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
        }
        else
        {
            PainelCorreto.SetActive(false);
            PainelErrado.SetActive(false);
            PainelFinal.SetActive(true);
            PainelFinal.transform.GetComponentInChildren<Text>().text = "Pontos: " + pontos.ToString();
        }
    }

    public void ClickResposta(int pos)
    {
        if (perguntaAtual.respostas[pos].ecorreta)
        {
            PainelCorreto.SetActive(true);
            pontos += perguntaAtual.pontos;
                
        }
        else
        {
            PainelErrado.SetActive(true);
        }

        carregarProximaPergunta = true;
    }

    public void IniciarQuiz()
    {
        SceneManager.LoadScene("Quiz");
    }

    public void MenuInicial()
    {
        SceneManager.LoadScene("MenuInicial");
    }

    public void Credito()
    {
        SceneManager.LoadScene("Credito");
    }

    public void SairJogo()
    {
        Application.Quit();
    }




}// FIM CLASS LOGICA